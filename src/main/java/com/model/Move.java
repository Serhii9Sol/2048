package com.model;

@FunctionalInterface
public interface Move {
    void move();
}
