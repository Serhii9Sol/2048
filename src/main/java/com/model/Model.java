package com.model;

import com.Main;
import com.view.View;
import java.util.*;

public class Model {

    public int score;
    public int maxTile;
    public boolean isGameWon;
    public boolean isGameLost;
    private Tile[][] gameTiles = new Tile[Main.FIELD_WIDTH][Main.FIELD_WIDTH];
    private Stack<Integer> previousScores;
    private Stack<Tile[][]> previousStates;
    private boolean isSaveNeeded = true;
    private View view;

    public Model(View view) {
        resetGameTiles();
        score = 0;
        maxTile = 0;
        isGameWon = false;
        isGameLost = false;
        previousScores = new Stack<Integer>();
        previousStates = new Stack<Tile[][]>();
        this.view = view;
    }

    public void repaint() {
        view.repaint();
    }

    public void wonMessage() {
        view.showWonMessage();
    }

    public void lostMessage() {
        view.showLostMessage();
    }

    public void resetGameTiles() {
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles.length; j++) {
                gameTiles[i][j] = new Tile();
            }
        }
        addTile();
        addTile();
    }

    public void left() {
        if (isSaveNeeded) {
            saveState(gameTiles);
        }
        boolean isChanged = false;
        for (int i = 0; i < Main.FIELD_WIDTH; i++) {
            if (compressTiles(gameTiles[i]) | mergeTiles(gameTiles[i])) {
                isChanged = true;
            }
        }
        if (isChanged) {
            isSaveNeeded = true;
            addTile();
        }
    }

    public void up() {
        saveState(gameTiles);
        turnRightGameTiles();
        turnRightGameTiles();
        turnRightGameTiles();
        left();
        turnRightGameTiles();
    }

    public void right() {
        saveState(gameTiles);
        turnRightGameTiles();
        turnRightGameTiles();
        left();
        turnRightGameTiles();
        turnRightGameTiles();
    }

    public void down() {
        saveState(gameTiles);
        turnRightGameTiles();
        left();
        turnRightGameTiles();
        turnRightGameTiles();
        turnRightGameTiles();
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }

    public boolean canMove() {
        if (!getEmptyTiles().isEmpty()) {
            return true;
        }
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 1; j < gameTiles.length; j++) {
                if (gameTiles[i][j].value == gameTiles[i][j - 1].value) {
                    return true;
                }
            }
        }
        for (int j = 0; j < gameTiles.length; j++) {
            for (int i = 1; i < gameTiles.length; i++) {
                if (gameTiles[i][j].value == gameTiles[i - 1][j].value) {
                    return true;
                }
            }
        }
        return false;
    }

    public void rollback() {
        if (!previousStates.empty() & !previousScores.empty()) {
            gameTiles = previousStates.pop();
            score = previousScores.pop();
        }
    }

    public void randomMove() {
        switch (((int) (Math.random() * 100)) % 4) {
            case 0:
                left();
                break;
            case 1:
                up();
                break;
            case 2:
                right();
                break;
            case 3:
                down();
                break;
        }
    }

    public void autoMove() {
        PriorityQueue<MoveEfficiency> queue = new PriorityQueue(4, Collections.reverseOrder());
        queue.add(getMoveEfficiency(this::left));
        queue.add(getMoveEfficiency(this::right));
        queue.add(getMoveEfficiency(this::up));
        queue.add(getMoveEfficiency(this::down));
        Move move = queue.peek().getMove();
        move.move();
    }

    private boolean compressTiles(Tile[] tiles) {
        boolean isArrayChange = false;
        Tile temp;
        for (int i = 0; i < tiles.length - 1; i++) {
            for (int j = 0; j < tiles.length - 1; j++) {
                if (tiles[j].isEmpty() && !tiles[j + 1].isEmpty()) {
                    temp = tiles[j];
                    tiles[j] = tiles[j + 1];
                    tiles[j + 1] = temp;
                    isArrayChange = true;
                }
            }
        }
        return isArrayChange;
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean isArrayChange = false;
        for (int i = 0; i < tiles.length - 1; i++) {
            if (!tiles[i].isEmpty() & tiles[i].value == tiles[i + 1].value) {
                int mergetTile = tiles[i].value * 2;
                tiles[i].value = mergetTile;
                tiles[i + 1].value = 0;
                if (maxTile < mergetTile) {
                    maxTile = mergetTile;
                }
                score += mergetTile;
                i++;
                isArrayChange = true;
            }
        }
        if (isArrayChange) {
            compressTiles(tiles);
        }
        return isArrayChange;
    }

    private List<Tile> getEmptyTiles() {
        List<Tile> emptyTiles = new ArrayList<>();
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles.length; j++) {
                if (gameTiles[i][j].isEmpty()) {
                    emptyTiles.add(gameTiles[i][j]);
                }
            }
        }
        return emptyTiles;
    }

    private void addTile() {
        List<Tile> emptyTiles = getEmptyTiles();
        if (!emptyTiles.isEmpty()) {
            emptyTiles.get((int) (emptyTiles.size() * Math.random())).value = Math.random() < 0.9 ? 2 : 4;
        }
    }

    private void turnRightGameTiles() {
        int[][] valueBufferedTiles = new int[Main.FIELD_WIDTH][Main.FIELD_WIDTH];

        for (int i = 0; i < Main.FIELD_WIDTH; i++) {
            for (int j = 0; j < Main.FIELD_WIDTH; j++) {
                valueBufferedTiles[j][Main.FIELD_WIDTH - 1 - i] = gameTiles[i][j].value;
            }
        }

        for (int i = 0; i < Main.FIELD_WIDTH; i++) {
            for (int j = 0; j < Main.FIELD_WIDTH; j++) {
                gameTiles[i][j].value = valueBufferedTiles[i][j];
            }
        }
    }

    private void saveState(Tile[][] gameTiles) {
        Tile[][] bufferArray = new Tile[gameTiles.length][gameTiles[0].length];
        int bufferScore = score;
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles[0].length; j++) {
                bufferArray[i][j] = new Tile(gameTiles[i][j].value);
            }
        }
        previousStates.push(bufferArray);
        previousScores.push(bufferScore);
        isSaveNeeded = false;
    }

    private boolean hasBoardChanged() {
        int currentSumOfTiles = 0;
        int previousSumOfTiles = 0;
        Tile[][] previousGameTiles = previousStates.peek();
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles[0].length; j++) {
                currentSumOfTiles += gameTiles[i][j].value;
                previousSumOfTiles += previousGameTiles[i][j].value;
            }
        }
        return currentSumOfTiles != previousSumOfTiles;
    }

    private MoveEfficiency getMoveEfficiency(Move move) {
        MoveEfficiency moveEfficiency;
        move.move();
        if (hasBoardChanged()) {
            moveEfficiency = new MoveEfficiency(getEmptyTiles().size(), score, move);
        } else {
            moveEfficiency = new MoveEfficiency(-1, 0, move);
        }
        rollback();
        return moveEfficiency;
    }
}
