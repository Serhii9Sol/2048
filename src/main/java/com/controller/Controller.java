package com.controller;

import com.Main;
import com.model.Model;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Controller extends KeyAdapter {

    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void resetGame() {
        model.score = 0;
        model.isGameLost = false;
        model.isGameWon = false;
        model.resetGameTiles();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            resetGame();
        } else {
            if (!model.canMove()) {
                model.isGameLost = true;
            } else {
                if (!model.isGameLost && !model.isGameWon) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_LEFT : model.left(); break;
                        case KeyEvent.VK_RIGHT : model.right(); break;
                        case KeyEvent.VK_UP : model.up(); break;
                        case KeyEvent.VK_DOWN : model.down(); break;
                        case KeyEvent.VK_Z : model.rollback(); break;
                        case KeyEvent.VK_R : model.randomMove(); break;
                        case KeyEvent.VK_A : model.autoMove(); break;
                    }
                }
                if (model.maxTile == Main.WINNING_TILE) {
                    model.isGameWon = true;
                }
            }
        }
        model.repaint();
        if (model.isGameWon) {
            model.wonMessage();
        } else if (model.isGameLost) {
            model.lostMessage();
        }
    }
}
