package com.model;

import java.awt.*;

public class Tile {

    public int value;

    public Tile() {
        this.value = 0;
    }

    public Tile(int value) {
        this.value = value;
    }

    public boolean isEmpty() {
        return this.value == 0;
    }

    public Color getFontColor() {
        return this.value < 16 ? new Color(0x776e65) : new Color(0xf9f6f2);
    }

    public Color getTileColor() {
        Color tileColor;
        switch (this.value) {
            case 0:
                tileColor = new Color(0xcdc1b4);
                break;
            case 2:
                tileColor = new Color(0xeee4da);
                break;
            case 4:
                tileColor = new Color(0xede0c8);
                break;
            case 8:
                tileColor = new Color(0xf2b179);
                break;
            case 16:
                tileColor = new Color(0xf59563);
                break;
            case 32:
                tileColor = new Color(0xf67c5f);
                break;
            case 64:
                tileColor = new Color(0xf65e3b);
                break;
            case 128:
                tileColor = new Color(0xedcf72);
                break;
            case 256:
                tileColor = new Color(0xedcc61);
                break;
            case 512:
                tileColor = new Color(0xedc850);
                break;
            case 1024:
                tileColor = new Color(0xedc53f);
                break;
            case 2048:
                tileColor = new Color(0xedc22e);
                break;
            default:
                tileColor = new Color(0xff0000);
                break;
        }
        return tileColor;
    }
}
