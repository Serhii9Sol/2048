package com;

import com.controller.Controller;
import com.model.Model;
import com.view.View;
import javax.swing.*;
import java.awt.*;

public class Main {
    public static final int WINNING_TILE = 2048;
    public static final int FIELD_WIDTH = 4;
    public static final int TILE_SIZE = 96;
    public static final int TILE_MARGIN = 12;
    public static final int SCORE_SIZE = 30;
    public static final Color BG_COLOR = new Color(0xbbada0);
    public static final String FONT_NAME = "Arial";

    public static void main(String[] args) {
        View view = new View();
        Model model = new Model(view);
        Controller controller = new Controller(model);
        view.setModel(model);

        JFrame game = new JFrame();
        game.addKeyListener(controller);
        game.add(view);
        game.setTitle("2048 Reset(Esc) Back(Z) Random(R) BestMove(A)");
        game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        game.setSize(FIELD_WIDTH * (TILE_SIZE + TILE_MARGIN) + (2 * TILE_MARGIN),
                FIELD_WIDTH * (TILE_SIZE + TILE_MARGIN) + (4 * TILE_MARGIN) + SCORE_SIZE);
        game.setResizable(false);
        game.setLocationRelativeTo(null);
        game.setVisible(true);
        game.setFocusable(true);
    }
}
