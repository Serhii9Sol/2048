package com.view;

import com.Main;
import com.model.Model;
import com.model.Tile;
import javax.swing.*;
import java.awt.*;

public class View extends JPanel {

    private Model model;

    public View() {

    }

    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Main.BG_COLOR);
        g.fillRect(0, 0, this.getSize().width, this.getSize().height);
        for (int x = 0; x < Main.FIELD_WIDTH; x++) {
            for (int y = 0; y < Main.FIELD_WIDTH; y++) {
                drawTile(g, model.getGameTiles()[y][x], x, y);
            }
        }
        g.drawString("Score: " + model.score, this.getSize().width / 2 - 75,
                Main.FIELD_WIDTH * (Main.TILE_SIZE + Main.TILE_MARGIN) + Main.SCORE_SIZE);
    }

    public void showWonMessage() {
        JOptionPane.showMessageDialog(this, "You've won!");
    }

    public void showLostMessage() {
        JOptionPane.showMessageDialog(this, "You've lost :( \n Esc to restart.");
    }

    private static int offsetCoors(int arg) {
        return arg * (Main.TILE_MARGIN + Main.TILE_SIZE) + Main.TILE_MARGIN;
    }

    private void drawTile(Graphics g2, Tile tile, int x, int y) {
        Graphics2D g = ((Graphics2D) g2);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int value = tile.value;
        int xOffset = offsetCoors(x);
        int yOffset = offsetCoors(y);
        g.setColor(tile.getTileColor());
        g.fillRoundRect(xOffset, yOffset, Main.TILE_SIZE, Main.TILE_SIZE , 8, 8);
        g.setColor(tile.getFontColor());
        final int size = value < 100 ? 36 : value < 1000 ? 32 : 24;
        final Font font = new Font(Main.FONT_NAME, Font.BOLD, size);
        g.setFont(font);

        String s = String.valueOf(value);
        final FontMetrics fm = getFontMetrics(font);

        final int w = fm.stringWidth(s);
        final int h = -(int) fm.getLineMetrics(s, g).getBaselineOffsets()[2];

        if (value != 0) {
            g.drawString(s, xOffset + (Main.TILE_SIZE - w) / 2, yOffset + Main.TILE_SIZE - (Main.TILE_SIZE - h) / 2 - 2);
        }
    }
}
